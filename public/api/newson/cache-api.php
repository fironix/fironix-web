<?php

class cacheApi
{
    const DB_SERVER = "";
    const DB_USER = "fironixc_db";
    const DB_PASSWORD = "db@nix";
    const DB = "fironixc_newson";
    
    public function __construct()
    {
        $this->dbConnect();// Initiate Database connection
    }

    //Database connection
    private function dbConnect() 
    {
        $this->db = mysqli_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD,self::DB);

        // Check connection
        if (mysqli_connect_errno())
        {
            http_response_code(404);
        }
        else 
        {
            $this->getData();
        }
    }
    
    private function getData()
    {
        
        if(isset($_GET['param'])) {
            $param = $_GET['param'];
            $type = $_GET['type'];

            $last_time = 'last_time';
            
            $curr_date = new DateTime();
            $date = date('Y-m-d H:i:s');

   
            $sql = "SELECT * FROM api_cache WHERE param = '$param'";
            $result = mysqli_query($this->db, $sql);

        
            if(mysqli_num_rows($result) > 0)
            {
                $result = mysqli_fetch_assoc($result);
                $last_date = new DateTime($result[$last_time]);  

                $interval = $last_date->diff($curr_date);

                $minutes = $interval->days * 24 * 60;
                $minutes += $interval->h * 60;
                $minutes += $interval->i;

                if ($minutes > 15) {

                    $ch = curl_init();
                    // Disable SSL verification
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    // Will return the response, if false it print the response
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    // Set the url
                    // curl_setopt($ch, CURLOPT_URL,'localhost/response.php');
                    curl_setopt($ch, CURLOPT_URL,'https://newsapi.org/v2/top-headlines?'.$type.'='.$param.'&apiKey=52dacafac9904a21bd1a401568af9738');
                    // Execute
                    $response = curl_exec($ch);
                    // Closing
                    curl_close($ch);

                    echo $response;


                    $sql = "UPDATE api_cache SET response = '".mysqli_real_escape_string($this->db, $response)."' , last_time = '$date' WHERE param = '$param'";
                    $result = mysqli_query($this->db, $sql);
                } else {
                    
                    $response = 'response';
                    echo $result[$response];
                }

            }
            else
            {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL,'https://newsapi.org/v2/top-headlines?'.$type.'='.$param.'&apiKey=52dacafac9904a21bd1a401568af9738');
                $response = curl_exec($ch);
                curl_close($ch);

                echo $response;

                $sql = "INSERT INTO api_cache (param, response, last_time) VALUES ('$param', '".mysqli_real_escape_string($this->db, $response)."', '$date')";
                $result = mysqli_query($this->db, $sql);
            }
        }
    }
    
}
    
$api = new cacheApi;