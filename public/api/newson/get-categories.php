<?php

include '../sec/MCrypt.php';

class getNewsSources
{
    const DB_SERVER = "";
    const DB_USER = "fironixc_db";
    const DB_PASSWORD = "db@nix";
    const DB = "fironixc_newson";
    
    public function __construct()
    {
        $this->dbConnect();// Initiate Database connection
    }

    //Database connection
    private function dbConnect() 
    {
        $this->db = mysqli_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD,self::DB);

        // Check connection
        if (mysqli_connect_errno())
        {
            http_response_code(404);
        }
        else 
        {
            $this->getData();
        }
    }
    
    private function getData()
    {
        $mcrypt = new MCrypt();
        
        $sql = "SELECT * FROM categories ORDER BY name";
        $result = mysqli_query($this->db, $sql);

        while ($row = mysqli_fetch_assoc($result)) 
                {
                    $newsArr[] = $row;
                }

        // echo json_encode($newsArr);  
        echo $mcrypt->encrypt(json_encode($newsArr));
        
    }
    
}
    
$api = new getNewsSources;
 
