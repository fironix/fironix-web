<?php

class GetSavePhotos
{
    const DB_SERVER = "";
    const DB_USER = "fironixc_db";
    const DB_PASSWORD = "db@nix";
    const DB = "fironixc_wallsapp";

    private $db = NULL;
    private $method = NULL;
    
    public function __construct()
    {
        $this->dbConnect();// Initiate Database connection
    }

    //Database connection
    private function dbConnect() 
    {
        $this->db = mysqli_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD,self::DB);

        // Check connection
        if (mysqli_connect_errno())
        {
            http_response_code(404);
        }
        else 
        {
            $this->getSaveUserPhotos();
        }
    }
    
    private function getSaveUserPhotos()
    {  
        if($_SERVER['REQUEST_METHOD']=='POST'){
 
            $u_id = $_POST['u_id'];
            $data = $_POST['data'];
            
            $sql = "SELECT u_id FROM photos WHERE u_id = '$u_id' LIMIT 1"; 

            $result = mysqli_query($this->db, $sql);
    
            if (!$result) {
                http_response_code(404);
                die(mysqli_error());
            }
    
            if(mysqli_num_rows($result) > 0)
            {
                $sql = "UPDATE photos SET data = '$data' WHERE u_id = '$u_id'";
                $result = mysqli_query($this->db, $sql);
            }
            else
            {
                $sql = "INSERT INTO photos (u_id, data) VALUES ('$u_id', '$data')";
                $result = mysqli_query($this->db, $sql);
            }
            
        }
        else if($_SERVER['REQUEST_METHOD']=='GET')
        {
            $u_id = $_GET['u_id'];
            
            $sql = "SELECT data FROM photos WHERE u_id = '$u_id' LIMIT 1";

            $result = mysqli_query($this->db, $sql);

            if (!$result) {
                http_response_code(401);
                die(mysqli_error());
            }

            if(mysqli_num_rows($result) > 0)
            {
                $result = mysqli_fetch_assoc($result);
                echo $result['data'];    
                
            }
        }
    
    }
    
}
    
$api = new GetSavePhotos;
 
