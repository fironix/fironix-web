<?php

include '../sec/MCrypt.php';
 
class RestApi
{
    const DB_SERVER = "";
    const DB_USER = "fironixc_db";
    const DB_PASSWORD = "db@nix";
    const DB = "fironixc_uplay";

    private $db = NULL;
    private $method = NULL;

    private $response = NULL;

    public function __construct()
    {
        $this->dbConnect();// Initiate Database connection
    }

    //Database connection
    private function dbConnect() 
    {
        $this->db = mysqli_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD,self::DB);

        // Check connection
        if (mysqli_connect_errno())
        {
            http_response_code(404);
        }
        else 
        {
            $this->findFunction();
        }
    }

    private function findFunction()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];

        switch ($this->method) {
            case 'GET':
                $this->fetchAll();
                break;

            case 'POST':
                break;

            case 'PUT':
                break;

            case 'DELETE':
                break;
        }   
    }

    private function fetchAll()
    {
        $packageName = $_GET['packageName'];

        if ($packageName == "live") {
            $this->fetchLive("live");
        } elseif ($packageName == "movies") {
            $this->fetchMovies("movies");
        } elseif ($packageName == "music") {
            $this->fetchMusic("music");
        } elseif ($packageName == "news") {
            $this->fetchNews("news");
        } elseif ($packageName == "tv") {
            $this->fetchTv("tv");
        } elseif ($packageName == "radio") {
           $this->fetchRadio("radio");
        }
    }

    private function fetchLive($packageName) 
    {
        $sql = "SELECT * FROM live ORDER BY id DESC";
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            http_response_code(404);
            die(mysqli_error());
        }

        if(mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) 
            {
                $moviesArr[] = $row;
            }
            $this->jsonEncode($moviesArr, "ok", $packageName);
        }
        else {
            $this->jsonEncode(null, "fail", $packageName);
        }  
    }

    private function fetchMovies($packageName) 
    {
        if (isset($_GET['movieType']) && $_GET['movieType'] == "all") {
            $sql = "SELECT * FROM movies_new UNION SELECT * FROM movies_animation ORDER BY id DESC";
        } else if (isset($_GET['movieType']) && $_GET['movieType'] == "new") {
            $sql = "SELECT * FROM movies_new ORDER BY id DESC";
        } else if (isset($_GET['movieType']) && $_GET['movieType'] == "collection") {
            $sql = "SELECT * FROM movies_collection ORDER BY id DESC";
        } else if (isset($_GET['movieType']) && $_GET['movieType'] == "animation") {
            $sql = "SELECT * FROM movies_animation ORDER BY id DESC";
        } else if (isset($_GET['movieType']) && $_GET['movieType'] == "tv_series") {
            $sql = "SELECT * FROM tv_series ORDER BY id DESC";
        } else {
            $sql = "SELECT * FROM movies_types ORDER BY id ASC";
        }
        
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            http_response_code(404);
            die(mysqli_error());
        }

        if(mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) 
            {
                $moviesArr[] = $row;
            }
            $this->jsonEncode($moviesArr, "ok", $packageName);
        }
        else {
            $this->jsonEncode(null, "fail", $packageName);
        }  
    }

    private function fetchMusic($packageName) 
    {
        $sql = "SELECT * FROM music ORDER BY id ASC";
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            http_response_code(404);
            die(mysqli_error());
        }

        if(mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) 
            {
                $moviesArr[] = $row;
            }
            $this->jsonEncode($moviesArr, "ok", $packageName);
        }
        else {
            $this->jsonEncode(null, "fail", $packageName);
        }  
    }

    private function fetchTv($packageName) 
    {
        if (isset($_GET['country'])) {
            $country = $_GET['country'];
            $sql = "SELECT * FROM tv_channels WHERE country ='$country' ORDER BY name ASC";
        } else{
            $sql = "SELECT * FROM tv_countries ORDER BY name ASC";
        }
        
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            http_response_code(404);
            die(mysqli_error());
        }

        if(mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) 
            {
                $moviesArr[] = $row;
            }
            $this->jsonEncode($moviesArr, "ok", $packageName);
        }
        else {
            $this->jsonEncode(null, "fail", $packageName);
        }  
    }

    private function fetchRadio($packageName) 
    {
        if (isset($_GET['country'])) {
            $country = $_GET['country'];
            $sql = "SELECT * FROM radio_channels WHERE country ='$country' ORDER BY name ASC";
        } else{
            $sql = "SELECT * FROM radio_countries ORDER BY name ASC";
        }

        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            http_response_code(404);
            die(mysqli_error());
        }

        if(mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) 
            {
                $moviesArr[] = $row;
            }
            $this->jsonEncode($moviesArr, "ok", $packageName);
        }
        else {
            $this->jsonEncode(null, "fail", $packageName);
        }  
    }

    private function fetchNews($packageName) 
    {
        if (isset($_GET['type'])) {
            $type = $_GET['type'];
            $sql = "SELECT * FROM $type";
        } else{
            $sql = "SELECT * FROM news_sources";
        }
        
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            http_response_code(404);
            die(mysqli_error());
        }

        if(mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) 
            {
                $moviesArr[] = $row;
            }
            $this->jsonEncode($moviesArr, "ok", $packageName);
        }
        else {
            $this->jsonEncode(null, "fail", $packageName);
        }  
    }

    private function jsonEncode($data, $status, $package)
    {
        $mcrypt = new MCrypt();
        
        $returnData['status'] = $status;
        $returnData['package'] = $package;

        if ($package == "news") {
            $returnData['apiKey'] = "fca0d90ddc1f4d31a4e5df5703a5ad96";
        }

        $returnData['data'] = $data;

        if(is_array($returnData)){
            // echo json_encode($returnData);
            echo $mcrypt->encrypt(json_encode($returnData));
        }
    }
}

$api = new RestApi;

