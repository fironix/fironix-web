<?php

class ApiFironixProfileImageUpload
{

    public function __construct()
    {
        $this->uploadImage();
    }

    private function uploadImage() 
    {
        if($_SERVER['REQUEST_METHOD']=='POST'){
 
            $image = $_POST['image'];

            $path = date("dmygis").".png";
            $actualpath = "https://fironix.com/profile_images/$path";
            
            if (file_put_contents("../../profile_images/$path",base64_decode($image))) {
                echo $actualpath;
            } else {
                echo "Error";
            }
            
         }else{
            echo "Error";
        }
        
    }
    
}
    
$api = new ApiFironixProfileImageUpload;
 
