<?php

class ApiFironixUserAppSettings
{
    const DB_SERVER = "";
    const DB_USER = "fironixc_admin";
    const DB_PASSWORD = "admin@nix";
    const DB = "fironixc_main";
    
    public function __construct()
    {
        $this->dbConnect();// Initiate Database connection
    }

    //Database connection
    private function dbConnect() 
    {
        $this->db = mysqli_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD,self::DB);

        // Check connection
        if (mysqli_connect_errno())
        {
            http_response_code(404);
        }
        else 
        {
            $this->userAppSettings();
        }
    }
    
    private function userAppSettings()
    {
        if($_SERVER['REQUEST_METHOD']=='POST'){
 
            $u_id = $_POST['u_id'];
            $name = $_POST['name'];
            $app_name = $_POST['app_name'];
            $settings = $_POST['settings'];
            
            $sql = "SELECT u_id FROM user_settings WHERE u_id = '$u_id' LIMIT 1"; 

            $result = mysqli_query($this->db, $sql);
    
            if (!$result) {
                http_response_code(404);
                die(mysqli_error());
            }
    
            if(mysqli_num_rows($result) > 0)
            {
                $sql = "UPDATE user_settings SET $app_name = '$settings', name = '$name' WHERE u_id = '$u_id'";
                $result = mysqli_query($this->db, $sql);
            }
            else
            {
                $sql = "INSERT INTO user_settings (u_id, name, $app_name) VALUES ('$u_id', '$name', '$settings')";
                $result = mysqli_query($this->db, $sql);
            }
            
        }
        else if($_SERVER['REQUEST_METHOD']=='GET')
        {
            $u_id = $_GET['u_id'];
            $app_name = $_GET['app_name'];
            
            $sql = "SELECT $app_name FROM user_settings WHERE u_id = '$u_id' LIMIT 1";

            $result = mysqli_query($this->db, $sql);

            if (!$result) {
                http_response_code(401);
                die(mysqli_error());
            }

            if(mysqli_num_rows($result) > 0)
            {
                $result = mysqli_fetch_assoc($result);
				
            	echo $result[$app_name];
                
                
            }
        }
    }
    
}
    
$api = new ApiFironixUserAppSettings;
 
