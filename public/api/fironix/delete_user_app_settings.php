<?php

class ApiFironixDeleteUserAppSettings
{
    const DB_SERVER = "";
    const DB_USER = "fironixc_admin";
    const DB_PASSWORD = "admin@nix";
    const DB = "fironixc_main";

    private $db = NULL;
    private $method = NULL;
    
    public function __construct()
    {
        $this->dbConnect();// Initiate Database connection
    }

    //Database connection
    private function dbConnect() 
    {
        $this->db = mysqli_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD,self::DB);

        // Check connection
        if (mysqli_connect_errno())
        {
            http_response_code(404);
        }
        else 
        {
            $this->deleteUserAppSettings();
        }
    }
    
    private function deleteUserAppSettings()
    {
        $u_id = $_POST['u_id'];
        
        $sql = "DELETE FROM user_settings WHERE u_id = '$u_id'";
        $result = mysqli_query($this->db, $sql);
            
            
        echo $u_id;
    }
    
}
    
$api = new ApiFironixDeleteUserAppSettings;
 
