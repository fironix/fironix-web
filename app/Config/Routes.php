<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->add('/privacy', 'Home::privacy');
$routes->get('/app/(:segment)', 'Home::app/$1');
$routes->get('/verify/(:segment)', 'Home::verify/$1');

$routes->group('api', function ($routes) {

	// User related APIs
	$routes->resource('user', ['controller' => 'Api\User']);
	$routes->post('user/social', 'Api\User::social');

	// Password related APIs
	$routes->post('password', 'Api\Password::send');
	$routes->put('password', 'Api\Password::reset');
	$routes->put('password/(:segment)', 'Api\Password::update/$1');

	// WallsApp related APIs
	$routes->get('wallsapp/(:segment)', 'Api\Wallsapp::show/$1');
	$routes->put('wallsapp/loved/(:segment)', 'Api\Wallsapp::updateLoved/$1');

	// NewsOn related APIs
	$routes->get('newson/sources', 'Api\Newson::getSources');
	$routes->get('newson/categories', 'Api\Newson::getCategories');
	$routes->get('newson/countries', 'Api\Newson::getCountries');
	$routes->get('newson/cache', 'Api\Newson::getCache');
	$routes->get('newson/(:segment)', 'Api\Newson::show/$1');
	$routes->put('newson/settings', 'Api\Newson::updateSettings');

	// UPlay related APIs
	$routes->get('uplay/(:segment)', 'Api\Uplay::show/$1');
	$routes->put('uplay/settings', 'Api\Uplay::updateSettings');
	
});

$routes->set404Override(function () {
	echo view('not_found.html');
});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
