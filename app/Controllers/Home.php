<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$appModel = model('App\Models\AppModel');

		$apps = $appModel->findAll();

		echo view('home', ['apps' => array_reverse($apps)]);
		echo view('footer');
	}

	public function privacy()
	{
		echo view('privacy');
		echo view('footer');
	}

	public function app($appName = null)
	{
		$appModel = model('App\Models\AppModel');

		$app = $appModel->where('short_name', $appName)
			->first();

		if (!$app) {
			return redirect()->to('/');
		}

		echo view('app', ['app' => $app]);
		echo view('footer');
	}

	public function verify($token = null)
	{
		$emailVerifyModel = model('App\Models\EmailVerifyModel');

		$emailVerify = $emailVerifyModel->where('token', $token)
			->first();

		$is_activated = false;

		if ($emailVerify) {
			$is_activated = true;

			$userModel = model('App\Models\UserModel');

			$userModel->update($emailVerify->user_id, ['verified' => 1]);

			$emailVerifyModel->delete($emailVerify->id);
		}

		echo view('verify', ['is_activated' => $is_activated]);
	}
}
