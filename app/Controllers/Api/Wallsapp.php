<?php

namespace App\Controllers\Api;

use CodeIgniter\API\ResponseTrait;

class Wallsapp extends \App\Controllers\BaseController
{
    use ResponseTrait;

    const STATUS = 'status';
    const MESSAGE = 'message';
    const NO_WALLSAPP = 'No wallsapp.';
    const WALLSAPP_FOUND = 'Wallsapp found.';
    const LOVED_SUCCESS = 'Loved photos successfully updated.';
    const LOVED_FAIL = 'Failed to update loved photos.';
    const AUTHENTICATION_FAILED = 'Authentication failed.';

    public function show($id = null)
    {
        helper('validation');
        if (!validateAuth($id, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $wallsAppModel = model('App\Models\WallsAppModel');

        $wallsApp = $wallsAppModel->where('user_id', $id)
            ->first();

        if (!$wallsApp) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_WALLSAPP], 400);
        }

        unset($wallsApp->created_at);
        unset($wallsApp->updated_at);

        return $this->respond([self::STATUS => true, self::MESSAGE => self::WALLSAPP_FOUND, 'wallsapp' => $wallsApp], 200);
    }

    public function updateLoved($id = null)
    {
        $loved = $this->request->getVar('loved');

        helper('validation');
        if (!validateAuth($id, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $wallsAppModel = model('App\Models\WallsAppModel');

        $wallsApp = $wallsAppModel->where('user_id', $id)
            ->first();

        if (!$wallsApp) {

            $data = [
                'user_id' => $id,
                'loved' => $loved
            ];

            $wallsAppModel->insert($data);
        } else {
            $wallsAppModel->update($wallsApp->id, ['loved' => $loved]);
        }

        if ($wallsAppModel->errors()) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::LOVED_FAIL], 400);
        }

        return $this->respond([self::STATUS => true, self::MESSAGE => self::LOVED_SUCCESS], 200);
    }
}
