<?php

namespace App\Controllers\Api;

use CodeIgniter\API\ResponseTrait;
use CodeIgniter\I18n\Time;

class Newson extends \App\Controllers\BaseController
{
    use ResponseTrait;

    const STATUS = 'status';
    const MESSAGE = 'message';
    const AUTHENTICATION_FAILED = 'Authentication failed.';
    const NO_NEWSON = 'No newson.';
    const NEWSON_FOUND = 'Newon found.';
    const SETTINGS_SUCCESS = 'Settings successfully updated.';
    const SETTINGS_FAIL = 'Failed to update settings.';
    const NO_SOURCES = 'No sources.';
    const SOURCES_FOUND = 'Sources found.';
    const NO_CATEGORIES = 'No categories.';
    const CATEGORIES_FOUND = 'Categories found.';
    const NO_COUNTRIES = 'No countries.';
    const COUNTRIES_FOUND = 'Countries found.';
    const NO_CACHE = 'No cache.';
    const CACHE_FOUND = 'Cache found.';

    public function show($userId = null)
    {
        helper('validation');
        if (!validateAuth($userId, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $newsOnModel = model('App\Models\NewsOnModel');

        $newsOn = $newsOnModel->where('user_id', $userId)
            ->first();

        if (!$newsOn) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_NEWSON], 400);
        }

        unset($newsOn->created_at);
        unset($newsOn->updated_at);

        return $this->respond([self::STATUS => true, self::MESSAGE => self::NEWSON_FOUND, 'newson' => $newsOn], 200);
    }

    public function updateSettings()
    {
        $userId = $this->request->getVar('user_id');
        $settings = $this->request->getVar('settings');

        helper('validation');
        if (!validateAuth($userId, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $newsOnModel = model('App\Models\NewsOnModel');

        $newsOn = $newsOnModel->where('user_id', $userId)
            ->first();

        if (!$newsOn) {

            $data = [
                'user_id' => $userId,
                'settings' => $settings
            ];

            $newsOnModel->insert($data);
        } else {
            $newsOnModel->update($newsOn->id, ['settings' => $settings]);
        }

        if ($newsOnModel->errors()) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::SETTINGS_FAIL], 400);
        }

        return $this->respond([self::STATUS => true, self::MESSAGE => self::SETTINGS_SUCCESS], 200);
    }

    public function getSources()
    {
        helper('validation');
        if (!validateNewsOnAuth($this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $newsOnSourceModel = model('App\Models\NewsOnSourceModel');

        $newsOnSources = $newsOnSourceModel->findAll();

        if (!$newsOnSources) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_SOURCES], 400);
        }

        return $this->respond([self::STATUS => true, self::MESSAGE => self::SOURCES_FOUND, 'sources' => $newsOnSources], 200);
    }

    public function getCategories()
    {
        helper('validation');
        if (!validateNewsOnAuth($this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $newsOnCategoryModel = model('App\Models\NewsOnCategoryModel');

        $newsOnCategories = $newsOnCategoryModel->findAll();

        if (!$newsOnCategories) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_CATEGORIES], 400);
        }

        return $this->respond([self::STATUS => true, self::MESSAGE => self::CATEGORIES_FOUND, 'categories' => $newsOnCategories], 200);
    }

    public function getCountries()
    {
        helper('validation');
        if (!validateNewsOnAuth($this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $newsOnCountryModel = model('App\Models\NewsOnCountryModel');

        $newsOnCountries = $newsOnCountryModel->findAll();

        if (!$newsOnCountries) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_COUNTRIES], 400);
        }

        return $this->respond([self::STATUS => true, self::MESSAGE => self::COUNTRIES_FOUND, 'countries' => $newsOnCountries], 200);
    }

    public function getCache()
    {
        $type = $this->request->getVar('type', FILTER_SANITIZE_STRING);
        $tag = $this->request->getVar('tag', FILTER_SANITIZE_STRING);

        helper('validation');
        if (!validateNewsOnAuth($this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $newsOnCacheModel = model('App\Models\NewsOnCacheModel');

        $newsOnCache = $newsOnCacheModel->where('tag', $tag)
            ->first();

        if (!$newsOnCache) {

            $response = $this->getNews($type, $tag);

            $data = [
                'tag' => $tag,
                'response' => $response
            ];

            $id = $newsOnCacheModel->insert($data);

            $newsOnCache = $newsOnCacheModel->find($id);
            unset($newsOnCache->created_at);
            unset($newsOnCache->updated_at);

            return $this->respond([self::STATUS => true, self::MESSAGE => self::CACHE_FOUND, 'cache' => $newsOnCache], 200);
        }

        $last_time = strtotime($newsOnCache->updated_at);
        $current_time = strtotime(date("Y-m-d H:i:s"));

        $minutes = round(abs($current_time - $last_time) / 60, 0);

        if ($minutes < 30) {
            unset($newsOnCache->created_at);
            unset($newsOnCache->updated_at);

            return $this->respond([self::STATUS => true, self::MESSAGE => self::CACHE_FOUND, 'cache' => $newsOnCache], 200);
        }

        $response = $this->getNews($type, $tag);

        $data = [
            'tag' => $tag,
            'response' => $response
        ];

        $newsOnCacheModel->update($newsOnCache->id, $data);

        $newsOnCache = $newsOnCacheModel->find($newsOnCache->id);
        unset($newsOnCache->created_at);
        unset($newsOnCache->updated_at);

        return $this->respond([self::STATUS => true, self::MESSAGE => self::CACHE_FOUND, 'cache' => $newsOnCache], 200);
    }

    public function getNews($type, $tag)
    {
        $ch = curl_init();
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set User-Agent
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0');
        // Set the url
        curl_setopt($ch, CURLOPT_URL, 'https://newsapi.org/v2/top-headlines?' . $type . '=' . $tag . '&apiKey=52dacafac9904a21bd1a401568af9738');
        // Execute
        $response = curl_exec($ch);
        // Closing
        curl_close($ch);

        return $response;
    }
}
