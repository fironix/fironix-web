<?php

namespace App\Controllers\Api;

use CodeIgniter\API\ResponseTrait;

class Uplay extends \App\Controllers\BaseController
{
    use ResponseTrait;

    const STATUS = 'status';
    const MESSAGE = 'message';
    const AUTHENTICATION_FAILED = 'Authentication failed.';
    const NO_UPLAY = 'No uplay.';
    const UPLAY_FOUND = 'Uplay found.';
    const SETTINGS_SUCCESS = 'Settings successfully updated.';
    const SETTINGS_FAIL = 'Failed to update settings.';

    public function show($userId = null)
    {
        helper('validation');
        if (!validateAuth($userId, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $uPlayModel = model('App\Models\UPlayModel');

        $uPlay = $uPlayModel->where('user_id', $userId)
            ->first();

        if (!$uPlay) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_UPLAY], 400);
        }

        unset($uPlay->created_at);
        unset($uPlay->updated_at);

        return $this->respond([self::STATUS => true, self::MESSAGE => self::UPLAY_FOUND, 'uplay' => $uPlay], 200);
    }

    public function updateSettings()
    {
        $userId = $this->request->getVar('user_id');
        $settings = $this->request->getVar('settings');

        helper('validation');
        if (!validateAuth($userId, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $uPlayModel = model('App\Models\UPlayModel');

        $uPlay = $uPlayModel->where('user_id', $userId)
            ->first();

        if (!$uPlay) {

            $data = [
                'user_id' => $userId,
                'settings' => $settings
            ];

            $uPlayModel->insert($data);
        } else {
            $uPlayModel->update($uPlay->id, ['settings' => $settings]);
        }

        if ($uPlayModel->errors()) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::SETTINGS_FAIL], 400);
        }

        return $this->respond([self::STATUS => true, self::MESSAGE => self::SETTINGS_SUCCESS], 200);
    }
}
