<?php

namespace App\Controllers\Api;

use CodeIgniter\API\ResponseTrait;

class User extends \App\Controllers\BaseController
{
    use ResponseTrait;

    const STATUS = 'status';
    const MESSAGE = 'message';
    const NO_ACCOUNT = 'No account for this email.';
    const USER_FOUND = 'User found.';
    const AUTHENTICATION_FAILED = 'Authentication failed.';
    const WRONG_PASSWORD = 'Wrong password.';
    const VERIFY_EMAIL_ADDRESS = 'Please verify your email address to activate account';
    const EMAIL_EXISTS = 'Sorry. That email has already been taken. Please choose another.';
    const ACCOUNT_SUCCESSFULL = 'Account successfully created. Please check your email and activate account.';
    const ACCOUNT_FAIL = 'Failed to create an account.';
    const ACCOUNT_DISABLED = 'Account disabled.';
    const SENDING_EMAIL = 'noreply@fironix.com';
    const ACTIVATE_TITLE = 'Verify your email for Fironix';
    const ACTIVATE_MESSAGE = 'Follow this link to verify your email address.';
    const ACTIVATE_LINK = 'https://fironix.com/verify/';
    const SIGNATURE = "Thanks,\nFironix Team";
    const INVALID_ACTION = 'Invalid action.';
    const UPDATE_SUCCESS = 'Account successfully updated.';
    const UPDATE_FAIL = 'Failed to update Account.';
    const INVALID_SOCIAL = 'Sorry. this email has already been taken for email login';

    public function index()
    {
        $email = $this->request->getVar('email', FILTER_SANITIZE_EMAIL);
        $password = $this->request->getVar('password', FILTER_SANITIZE_STRING);

        $userModel = model('App\Models\UserModel');

        $user = $userModel->where('email', $email)
            ->first();

        if (!$user) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_ACCOUNT], 404);
        }

        if (!password_verify($password, $user->password)) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::WRONG_PASSWORD], 401);
        }

        if ($user->verified != 1) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::VERIFY_EMAIL_ADDRESS], 400);
        }

        if ($user->status != 1) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::ACCOUNT_DISABLED], 400);
        }

        unset($user->password);
        unset($user->status);
        unset($user->verified);
        unset($user->created_at);
        unset($user->updated_at);

        $authTokenModel = model('App\Models\AuthTokenModel');

        $authToken = $authTokenModel->where('user_id', $user->id)
            ->first();

        if (!$authToken) {
            $token = bin2hex(openssl_random_pseudo_bytes(32));

            $data = [
                'user_id' => $user->id,
                'token' => $token
            ];

            $authTokenModel->insert($data);

            $this->response->setHeader('token', $token);
        } else {
            $this->response->setHeader('token', $authToken->token);
        }

        return $this->respond([self::STATUS => true, self::MESSAGE => self::USER_FOUND, 'user' => $user], 200);
    }

    public function create()
    {
        $email = $this->request->getVar('email', FILTER_SANITIZE_EMAIL);
        $firstName = $this->request->getVar('first_name', FILTER_SANITIZE_STRING);
        $lastName = $this->request->getVar('last_name', FILTER_SANITIZE_STRING);
        $password = $this->request->getVar('password', FILTER_SANITIZE_STRING);

        $data = [
            'email' => $email,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'type' => 'email',
            'password' => password_hash($password, PASSWORD_BCRYPT)
        ];

        $userModel = model('App\Models\UserModel');

        $user = $userModel->where('email', $email)
            ->first();

        if ($user) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::EMAIL_EXISTS], 400);
        }

        $id = $userModel->insert($data);

        if ($userModel->errors()) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::ACCOUNT_FAIL], 400);
        }

        $token = md5(rand());

        $data = [
            'user_id' => $id,
            'token' => $token
        ];

        $emailVerifyModel = model('App\Models\EmailVerifyModel');

        $emailVerifyModel->insert($data);

        $emailLibrary = \Config\Services::email();

        $emailLibrary->setFrom(self::SENDING_EMAIL, 'Fironix');
        $emailLibrary->setTo($email);

        $emailLibrary->setSubject(self::ACTIVATE_TITLE);
        $emailLibrary->setMessage("Hi " . $firstName . ".\n\n" . self::ACTIVATE_MESSAGE . "\n\n" . self::ACTIVATE_LINK . $token . "\n\n" . self::SIGNATURE);

        $emailLibrary->send();

        return $this->respond([self::STATUS => true, self::MESSAGE => self::ACCOUNT_SUCCESSFULL], 201);
    }

    public function social()
    {
        $email = $this->request->getVar('email', FILTER_SANITIZE_EMAIL);
        $firstName = $this->request->getVar('first_name', FILTER_SANITIZE_STRING);
        $lastName = $this->request->getVar('last_name', FILTER_SANITIZE_STRING);
        $type = $this->request->getVar('type', FILTER_SANITIZE_STRING);

        $data = [
            'email' => $email,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'type' => $type,
            'verified' => 1
        ];

        $userModel = model('App\Models\UserModel');

        $user = $userModel->where('email', $email)
            ->first();

        if ($user && $user->type == 'email') {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::INVALID_SOCIAL], 400);
        }

        if (!$user) {
            $userModel->insert($data);
        }

        if ($userModel->errors()) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::ACCOUNT_FAIL], 400);
        }

        $user = $userModel->where('email', $email)
            ->first();

        if ($user->status != 1) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::ACCOUNT_DISABLED], 400);
        }

        $authTokenModel = model('App\Models\AuthTokenModel');

        $authToken = $authTokenModel->where('user_id', $user->id)
            ->first();

        if (!$authToken) {
            $token = bin2hex(openssl_random_pseudo_bytes(32));

            $data = [
                'user_id' => $user->id,
                'token' => $token
            ];

            $authTokenModel->insert($data);

            $this->response->setHeader('token', $token);
        } else {
            $this->response->setHeader('token', $authToken->token);
        }

        unset($user->password);
        unset($user->status);
        unset($user->verified);
        unset($user->created_at);
        unset($user->updated_at);

        return $this->respond([self::STATUS => true, self::MESSAGE => self::USER_FOUND, 'user' => $user], 200);
    }

    public function show($id = null)
    {
        $userModel = model('App\Models\UserModel');

        helper('validation');
        if (!validateAuth($id, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $user = $userModel->find($id);

        if (!$user) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_ACCOUNT], 404);
        }

        unset($user->password);
        unset($user->status);
        unset($user->verified);
        unset($user->created_at);
        unset($user->updated_at);

        return $this->respond([self::STATUS => true, self::MESSAGE => self::USER_FOUND, 'user' => $user], 200);
    }

    public function update($id = null)
    {
        $userModel = model('App\Models\UserModel');

        helper('validation');
        if (!validateAuth($id, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $firstName = $this->request->getVar('first_name', FILTER_SANITIZE_STRING);
        $lastName = $this->request->getVar('last_name', FILTER_SANITIZE_STRING);

        $data = [
            'first_name' => $firstName,
            'last_name' => $lastName,
        ];

        $userModel->update($id, $data);

        if ($userModel->errors()) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::UPDATE_FAIL], 400);
        }

        return $this->respond([self::STATUS => true, self::MESSAGE => self::UPDATE_SUCCESS], 200);
    }

    public function delete($id = null)
    {
        echo "delete";
    }
}
