<?php

namespace App\Controllers\Api;

use CodeIgniter\API\ResponseTrait;

class Password extends \App\Controllers\BaseController
{
    use ResponseTrait;

    const STATUS = 'status';
    const MESSAGE = 'message';
    const NO_ACCOUNT = 'No account for this email.';
    const EMAIL_SENT = 'Reset password email sent.';
    const AUTHENTICATION_FAILED = 'Authentication failed.';
    const INVALID_CURRENT_PASSWORD = 'Invalid current passowrd.';
    const INVALID_RESET = 'Invalid reset password link.';
    const UPDATE_SUCCESS = 'Password successfully updated.';
    const ACCOUNT_DISABLED = 'Account disabled.';
    const VERIFY_EMAIL_ADDRESS = 'Please verify your email address to activate account';
    const INVALID_SOCIAL = 'Sorry. That email has already been taken for social login';

    const SENDING_EMAIL = 'noreply@fironix.com';
    const RESET_TITLE = 'Reset your password for Fironix';
    const RESET_MESSAGE = 'Follow this link to reset your Fironix password.';
    const RESET_MESSAGE_BOTTOM = 'If you didn\'t ask to reset your password, you can ignore this email.';
    const RESET_LINK = 'https://fironix.com/password/';
    const SIGNATURE = "Thanks,\nFironix Team";

    public function send()
    {
        $email = $this->request->getVar('email', FILTER_SANITIZE_EMAIL);

        $userModel = model('App\Models\UserModel');

        $user = $userModel->where('email', $email)
            ->first();

        if (!$user) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_ACCOUNT], 400);
        }

        if ($user->verified != 1) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::VERIFY_EMAIL_ADDRESS], 400);
        }

        if ($user->status != 1) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::ACCOUNT_DISABLED], 400);
        }

        if ($user->type != 'email') {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::INVALID_SOCIAL], 400);
        }

        $token = md5(rand());

        $passwordResetModel = model('App\Models\PasswordResetModel');

        $passwordReset = $passwordResetModel->where('user_id', $user->id)
            ->first();

        if (!$passwordReset) {

            $data = [
                'user_id' => $user->id,
                'token' => $token
            ];

            $passwordResetModel->insert($data);
        } else {
            $passwordResetModel->update($passwordReset->id, ['token' => $token]);
        }

        $emailLibrary = \Config\Services::email();

        $emailLibrary->setFrom(self::SENDING_EMAIL, 'Fironix');
        $emailLibrary->setTo($email);

        $emailLibrary->setSubject(self::RESET_TITLE);
        $emailLibrary->setMessage("Hi " . $user->first_name . ".\n\n"
            . self::RESET_MESSAGE . "\n\n"
            . self::RESET_LINK . $token . "\n\n"
            . self::RESET_MESSAGE_BOTTOM . "\n\n"
            . self::SIGNATURE);

        $emailLibrary->send();

        return $this->respond([self::STATUS => true, self::MESSAGE => self::EMAIL_SENT], 200);
    }

    public function update($id = null)
    {
        $currentPassword = $this->request->getVar('current_password', FILTER_SANITIZE_STRING);
        $newPassword = $this->request->getVar('new_password', FILTER_SANITIZE_STRING);

        helper('validation');
        if (!validateAuth($id, $this->request->getHeaderLine('token'))) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::AUTHENTICATION_FAILED], 401);
        }

        $userModel = model('App\Models\UserModel');

        $user = $userModel->where('id', $id)
            ->first();

        if (!$user) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::NO_ACCOUNT], 400);
        }

        if (!password_verify($currentPassword, $user->password)) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::INVALID_CURRENT_PASSWORD], 400);
        }

        $userModel->update($user->id, ['password' => password_hash($newPassword, PASSWORD_BCRYPT)]);

        return $this->respond([self::STATUS => true, self::MESSAGE => self::UPDATE_SUCCESS], 200);
    }

    public function reset()
    {
        $token = $this->request->getVar('token', FILTER_SANITIZE_STRING);
        $password = $this->request->getVar('password', FILTER_SANITIZE_STRING);

        $passwordResetModel = model('App\Models\PasswordResetModel');

        $passwordReset = $passwordResetModel->where('token', $token)
            ->first();

        if (!$passwordReset) {
            return $this->respond([self::STATUS => false, self::MESSAGE => self::INVALID_RESET], 400);
        }

        $userModel = model('App\Models\UserModel');
        $userModel->update($passwordReset->user_id, ['password' => password_hash($password, PASSWORD_BCRYPT)]);
        $passwordResetModel->delete($passwordReset->id);

        return $this->respond([self::STATUS => true, self::MESSAGE => self::UPDATE_SUCCESS], 200);
    }
}
