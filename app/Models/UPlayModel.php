<?php

namespace App\Models;

use CodeIgniter\Model;

class UPlayModel extends Model
{
    protected $table      = 'uplay';
    protected $primaryKey = 'id';

    protected $returnType = 'object';

    protected $allowedFields = ['user_id', 'settings'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [
        'user_id'   => 'required'
    ];

    protected $skipValidation     = false;
}
