<?php

namespace App\Models;

use CodeIgniter\Model;

class NewsOnSourceModel extends Model
{
    protected $table      = 'newson_sources';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
}
