<?php

namespace App\Models;

use CodeIgniter\Model;

class PasswordResetModel extends Model
{
    protected $table      = 'password_reset';
    protected $primaryKey = 'id';

    protected $returnType = 'object';

    protected $allowedFields = ['user_id', 'token'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [
        'user_id'   => 'required',
        'token'     => 'required'
    ];

    protected $skipValidation     = false;
}
