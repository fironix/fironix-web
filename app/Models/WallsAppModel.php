<?php

namespace App\Models;

use CodeIgniter\Model;

class WallsAppModel extends Model
{
    protected $table      = 'wallsapp';
    protected $primaryKey = 'id';

    protected $returnType = 'object';

    protected $allowedFields = ['user_id', 'loved'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [
        'user_id'   => 'required'
    ];

    protected $skipValidation     = false;
}
