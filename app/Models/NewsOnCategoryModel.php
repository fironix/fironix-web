<?php

namespace App\Models;

use CodeIgniter\Model;

class NewsOnCategoryModel extends Model
{
    protected $table      = 'newson_categories';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
}
