<?php

namespace App\Models;

use CodeIgniter\Model;

class NewsOnCountryModel extends Model
{
    protected $table      = 'newson_countries';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
}
