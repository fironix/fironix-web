<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthTokenModel extends Model
{
    protected $table      = 'auth_tokens';
    protected $primaryKey = 'id';

    protected $returnType = 'object';

    protected $allowedFields = ['user_id', 'token'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [
        'user_id'   => 'required',
        'token'     => 'required'
    ];

    protected $skipValidation     = false;
}
