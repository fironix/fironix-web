<?php

namespace App\Models;

use CodeIgniter\Model;

class NewsOnModel extends Model
{
    protected $table      = 'newson';
    protected $primaryKey = 'id';

    protected $returnType = 'object';

    protected $allowedFields = ['user_id', 'settings'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [
        'user_id'   => 'required'
    ];

    protected $skipValidation     = false;
}
