<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $returnType = 'object';

    protected $allowedFields = ['email', 'first_name', 'last_name', 'type', 'password', 'status', 'verified'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [
        'email'        => 'required|valid_email|is_unique[users.email]',
        'first_name'   => 'required',
        'type'         => 'required'
    ];

    protected $skipValidation     = false;
}
