<?php

namespace App\Models;

use CodeIgniter\Model;

class NewsOnCacheModel extends Model
{
    protected $table      = 'newson_cache';
    protected $primaryKey = 'id';

    protected $returnType = 'object';

    protected $allowedFields = ['tag', 'response'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [
        'tag'   => 'required',
        'response'   => 'required'
    ];

    protected $skipValidation     = false;
}
