<?php

const STATUS = 'status';
const MESSAGE = 'message';
const AUTHENTICATION_FAILED = 'Authentication failed.';

function validateAuth($id, $token)
{
    $authTokenModel = model('App\Models\AuthTokenModel');

    $authToken = $authTokenModel->where('user_id', $id)
        ->where('token', $token)
        ->first();

    if ($authToken) {
        return true;
    } else {
        return false;
    }
}

function validateNewsOnAuth($token)
{
    $authTokenModel = model('App\Models\AuthTokenModel');

    $authToken = $authTokenModel->where('token', $token)->first();

    if ($authToken) {
        return true;
    } else {
        return false;
    }
}
