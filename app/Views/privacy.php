<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ubuntu Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">

    <link rel="shortcut icon" type="image/png" href="../img/logo.png" />
    <link rel="stylesheet" type="text/css" href="../css/main.css">

    <title>Privacy</title>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="https://fironix.com">
                <img src="../img/logo_trans.png" alt="Logo" style="width:50px;">
            </a>
        </div>
    </nav>

    <div class="container jumbotron" style="text-align: center">

        <div>
            <h1>Privacy Policy</h1><hr><br>
            <p>This page is used to inform users regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</p>
            <p>If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p>
            <p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.</p>
        </div>

        <div style="margin-top: 80px">
            <h2>Collection of Your Information</h2><br>
            <h3>Personal Data</h3>
            <p>We collect First Name, Last Name and Email when you are going to create an account.</p></br>
            <h3>Data from Social Networks</h3>
            <p>User information from social networking sites(Google, Facebook), We use social network uername, email and profile picture(only public profile details) when user sign in.</p>
        </div>

        <div style="margin-top: 80px">
            <h2>Use of Your Information</h2><br>
            <p>We use your information only for creating an account. Then you can save your app settings to your account and get customized experience.</p>
        </div>

        <div style="margin-top: 80px">
            <h2>Security of Your Information</h2><br>
            <p>We are not sharing your account details with any third parties.</p>
        </div>

        <div style="margin-top: 80px">
            <h2>Device Permissions</h2>
            <p>Depending on the User's specific device, applications may request certain permissions that allow it to access the User's device Data as described below.</p>
            <p>By default, these permissions must be granted by the user before the respective information can be accessed. Once the permission has been given, it can be revoked by the user at any time. In order to revoke these permissions, users may refer to the device settings.<br />The exact procedure for controlling app permissions may be dependent on the user's device and software.</p>
            <p>If user grants any of the permissions listed below, the respective access may be processed by respective applications.</p>
        </div>

        <div style="margin-top: 50px;">
            <h3>Intenet permission</h3>
            <p>Used for accessing to internet.</p>
        </div>

        <div style="margin-top: 30px;">
            <h3>Storage permission</h3>
            <p>Used for accessing shared external storage, including the reading any items.</p>
        </div>

        <div style="margin-top: 30px;">
            <h3>Bluetooth sharing permission</h3>
            <p>Used for accessing Bluetooth related functions such as connecting with devices, and allowing data transfer between devices.</p>
        </div>

        <div style="margin-top: 30px;">
            <h3>Social media accounts permission</h3>
            <p>Used for accessing the User's social media account profiles, such as Google and Facebook.</p>
        </div>

        <div style="margin-top: 80px">
            <h2>Contact Us</h2><br>
            <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at contact@fironix.com.</p>
        </div>

    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="../js/main.js"></script>

</body>

</html>