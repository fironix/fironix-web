<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ubuntu Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">

    <link rel="shortcut icon" type="image/png" href="img/logo.png" />
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <title>Fironix</title>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

    <nav class="navbar navbar-expand-md fixed-top navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="img/logo_trans.png" alt="Logo" style="width:50px;">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="main-navigation">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#section_home">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section_new">NEW</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section_products">PRODUCTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section_contact">CONTACT</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header id="section_home" class="header container-fluid"></header>

    <div id="section_new">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2>NEW</h2>
                <i class="fa fa-get-pocket" style="color: #2c3e50; font-size: 1.5rem;"></i>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row section">

                <div class="col-lg-6 pt-5 pb-5" style="text-align: center;">
                    <div class="container">
                        <div id="carouselId" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">

                                <?php $images = json_decode($apps[0]->images);

                                $first = true;
                                foreach ($images as $image) :
                                    if ($first) { ?>
                                        <div class="carousel-item active">
                                            <img src="<?php echo $image->image; ?>" style="max-width: 40%;">
                                        </div>
                                    <?php $first = false;
                                    } else { ?>
                                        <div class="carousel-item">
                                            <img src="<?php echo $image->image; ?>" style="max-width: 40%;">
                                        </div>
                                <?php }
                                endforeach; ?>

                            </div>
                            <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">

                </div>
                <div class="col-lg-6" style="align-self: center; padding-right: 0px; padding-left: 0px;">
                    <div class="container section-new" style="padding-top: 2rem; padding-left: 5rem; padding-bottom: 2rem;">
                        <h3 style="margin-bottom: 2rem;"><?php echo $apps[0]->name; ?></h3>
                        <ul>
                            <?php $description = json_decode($apps[0]->description);

                            foreach ($description as $desc) : ?>
                                <li><?php echo $desc->desc; ?></li>
                            <?php endforeach; ?>
                        </ul>
                        <?php if ($apps[0]->web_site != "") { ?><a class="btn" style="color: #1abc9c; background-color: #fff; margin-top: 1rem; width: 100px; margin-right:1rem" href="<?php echo $apps[0]->web_site; ?>" role="button">Website</a><?php } ?>
                        <a class="btn" style="color: #1abc9c; background-color: #fff; margin-top: 1rem; width: 100px;" href="<?php echo $apps[0]->download; ?>" role="button">Install</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="section_products">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2>PRODUCTS</h2>
                <i class="fa fa-get-pocket" style="color: #2c3e50; font-size: 1.5rem;"></i>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <?php foreach ($apps as $app) : ?>

                    <div class="col-lg-3">
                        <a href="/app/<?php echo $app->short_name; ?>"><img src="<?php echo $app->app_image; ?>" style="max-width: 100%;"></a>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div id="section_contact">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2>CONTACT US</h2>
                <i class="fa fa-get-pocket" style="color: #2c3e50; font-size: 1.5rem;"></i>
                <p style="font-size: 15px; color: #2c3e50; padding-top: 1rem;">Email : <a href="mailto:info@fironix.com">info@fironix.com</a></p>
            </div>
        </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="js/main.js"></script>

</body>

</html>