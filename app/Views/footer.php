<div class="container-fluid section contact">
    <a href="/"><img src="/img/logo_trans.png" alt="Logo" style="width:50px;"></a>
    <h6>WE ARE SOCIAL</h6>
    <hr style="background-color: #fff; margin-top: 1rem; width: 200px;">
    <a href="https://facebook.com/ToFironix"><i class="fa fa-facebook" style="color: #fff; font-size: 1.2rem;"></i></a>
    <p style="color: #fff; margin-top: 3rem; font-size: 13px;">&copy; 2021 FIRONIX</p>
</div>