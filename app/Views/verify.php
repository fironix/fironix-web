<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ubuntu Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">

    <link rel="shortcut icon" type="image/png" href="../img/logo.png" />
    <link rel="stylesheet" type="text/css" href="../css/main.css">

    <title>Varify</title>
</head>

<body>

    <div class="container d-flex justify-content-center">
        <?php
        if ($is_activated == true) { ?>
            <div class="card jumbotron" style="width: 20rem; background-color: #3BDCDE; align-items: center; border: none">

                <img class="card-img-top" style="width: 5rem" src="../img/ok.png">

                <div class="card-body">
                    <h3 class="card-title text-white">Verified</h3>
                    <p class="card-text text-white">Your account has been activated.</p>
                </div>

            </div>
        <?php
        } else { ?>
            <div class="card jumbotron" style="width: 20rem; background-color: #ff5969; align-items: center; border: none">

                <img class="card-img-top" style="width: 5rem" src="../img/cancel.png">

                <div class="card-body">
                    <h3 class="card-title text-white">Failed</h3>
                    <p class="card-text text-white">Invalid link.</p>
                </div>

            </div>
        <?php
        } ?>

    </div>

    <div class="container-fluid section contact " style="position: fixed; bottom: 0">
        <a href="https://fironix.com"><img src="../img/logo_trans.png" alt="Logo" style="width:50px;"></a>
        <h6>WE ARE SOCIAL</h6>
        <hr style="background-color: #fff; margin-top: 1rem; width: 200px;">
        <a href="https://facebook.com/ToFironix"><i class="fa fa-facebook" style="color: #fff; font-size: 1.2rem;"></i></a>
        <p style="color: #fff; margin-top: 3rem; font-size: 13px;">&copy; 2020 Fironix</p>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="../js/main.js"></script>

</body>

</html>