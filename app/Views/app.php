<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ubuntu Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">

    <link rel="shortcut icon" type="image/png" href="/img/logo.png" />
    <link rel="stylesheet" type="text/css" href="/css/main.css">

    <title>Fironix</title>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="/img/logo_trans.png" alt="Logo" style="width:50px;">
            </a>
        </div>
    </nav>

    <div id="section_new">
        <div class="container-fluid">
            <div class="row section">
                <div class="col-lg-6 pt-5 pb-5" style="text-align: center;">
                    <div class="container">
                        <div id="carouselId" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">

                                <?php $images = json_decode($app->images);

                                $first = true;
                                foreach ($images as $image) :
                                    if ($first) { ?>
                                        <div class="carousel-item active">
                                            <img src="<?php echo $image->image; ?>" style="max-width: 40%;">
                                        </div>
                                    <?php $first = false;
                                    } else { ?>
                                        <div class="carousel-item">
                                            <img src="<?php echo $image->image; ?>" style="max-width: 40%;">
                                        </div>
                                <?php }
                                endforeach; ?>

                            </div>
                            <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">

                </div>
                <div class="col-lg-6" style="align-self: center; padding-right: 0px; padding-left: 0px;">
                    <div class="container section-new" style="padding-top: 2rem; padding-left: 5rem; padding-bottom: 2rem;">
                        <h3 style="margin-bottom: 2rem;"><?php echo $app->name; ?></h3>
                        <ul>
                            <?php $description = json_decode($app->description);

                            foreach ($description as $desc) : ?>
                                <li><?php echo $desc->desc; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div>
                <?php if ($app->web_site != "") { ?><a class="btn" style="color: #fff; background-color: #1abc9c; margin-bottom: 1rem; width: 100px;" href="<?php echo $app->web_site; ?>" role="button">Website</a><?php } ?>
            </div>
            <div>
                <a class="btn" style="color: #fff; background-color: #1abc9c; width: 100px;" href="<?php echo $app->download; ?>" role="button">Install</a>
            </div>
        </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="/js/main.js"></script>

</body>

</html>